**NodeWithMongo**

**MongoDB簡介**

- No SQL  非關連式資料庫，沒有schema
- 用來處理大量dat並且以key:value表示（跟JSON黨一樣）
- 以文件導向儲存(Document Oriented Storage)的資料庫
- 沒有複雜的關聯
- 使用內部記憶體儲存工作集，進而實現更快的數據訪問
- 單一物件結構清楚
- 不需要應用程式與資料庫物件之間的轉換與對應

**安裝流程**

1. mongodb官網下載檔案．並且解壓縮安裝
2. 在mongodb資料夾內建立存放ㄉdb的資料夾
3. cd 至 mongodb/bin 資料夾
4. 在cmd打 mongod --dbpath db存放位置的資料夾
5. 可以在打開另外一個cmd  輸入mongo.exe的存放位置路徑(看到 connecting to: test. Welcome to the MongoDB shello 即連線成功)

**Node連接Mongodb**

```
var MongoClient = require('mongodb').MongoClient;
// Connect to the db
MongoClient.connect("mongodb://localhost:27017/mymondb", function (err, db) {
  if(err) throw err;
  //Write databse Insert/Update/Query code here..
  console.log('mongodb is running!');
  db.close(); //關閉連線
});
```
- 利用node提供module 連接至mongodb
- 預測mongodb之port為27017，斜線後面為資料酷名稱（若沒有該資料庫名稱則mondb會自己創造一個）
- 設立callback function ，之後就可以在裡面新增修改刪除查詢mongodb內的資料(也可以使用mogodbshell動作)


**Mongodb基本action**

- show dbs (查看目前有哪些資料庫)
- use  db_name (使用哪個db，若沒有那個db name 則會創造一個)
- db.collection_name.find (可以查看collection 裡面的資料)
- db.droupDatabase()（刪除db)
- db.createCollection(name) （創造collection）
- db.collection.insert() (插入資料 有分insetOne/insertMany)
- db.collection.delete() (刪除資料 有分deleteOne/deleteMany)
- db.collection.remove() (刪除資料)
- db.collection.update() (更新資料)
[語法查詢處：(https://docs.mongodb.com/v3.4/reference/method/js-collection/)
](https://docs.mongodb.com/v3.4/reference/method/js-collection/)

